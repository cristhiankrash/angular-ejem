import { NgModule } from '@angular/core';
import { ProductRoutingModule } from './product-routing.module';

import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { ProductComponent } from './component/product/product.component';
import { ProductDetailComponent } from './component/product-detail/product-detail.component';
import { ProductsComponent } from './component/products/products.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [
    ProductComponent,
    ProductDetailComponent,
    ProductsComponent
  ],
  // exports: [
  //   ProductComponent,
  //   ProductDetailComponent,
  //   ProductsComponent
  // ],
  imports: [
    CommonModule,
    SharedModule,
    ProductRoutingModule,
    MaterialModule
  ]
})
export class ProductsModule { }
